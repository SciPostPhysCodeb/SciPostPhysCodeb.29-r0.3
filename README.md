# Codebase release r0.3 for SmoQyDQMC.jl

by Benjamin Cohen-Stead, Sohan Malkaruge Costa, James Neuhaus, Andy Tanjaroon Ly, Yutan Zhang, Richard Scalettar, Kipton Barros, Steven Johnston

SciPost Phys. Codebases 29-r0.3 (2024) - published 2024-05-28

[DOI:10.21468/SciPostPhysCodeb.29-r0.3](https://doi.org/10.21468/SciPostPhysCodeb.29-r0.3)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.29-r0.3) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Benjamin Cohen-Stead, Sohan Malkaruge Costa, James Neuhaus, Andy Tanjaroon Ly, Yutan Zhang, Richard Scalettar, Kipton Barros, Steven Johnston.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.29-r0.3](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.29-r0.3)
* Live (external) repository at [https://github.com/SmoQySuite/SmoQyDQMC.jl/tree/v0.3.6](https://github.com/SmoQySuite/SmoQyDQMC.jl/tree/v0.3.6)
